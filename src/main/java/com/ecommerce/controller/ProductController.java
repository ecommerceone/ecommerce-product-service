package com.ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.entity.ProductInfo;
import com.ecommerce.model.ProductModel;
import com.ecommerce.service.ProductService;

@RestController
@RequestMapping("productservice")
public class ProductController {

	@Autowired
	ProductService productService;
	
	@PostMapping("product")
	ResponseEntity<ProductInfo> addProduct(@RequestBody ProductModel productModel){
		
		ProductInfo productInfo = productService.addProduct(productModel);
		return new ResponseEntity<>(productInfo, HttpStatus.OK);
	}
	
	@GetMapping("products")
	public ResponseEntity<List<ProductInfo>> getAllProducts() {
		List<ProductInfo> products = productService.getAllProducts();
		return new ResponseEntity<>(products, HttpStatus.OK);
	}
	
	@GetMapping("product/{productId}")
	public ResponseEntity<ProductInfo> getProductById(@PathVariable Long productId) {
		ProductInfo productInfo = productService.getProductById(productId);
		return new ResponseEntity<>(productInfo, HttpStatus.OK);
		
	}
	
}
