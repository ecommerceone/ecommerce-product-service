package com.ecommerce.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class ProductInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long productId;

	private String productName;
	
	private String productDescription;
	
	private String productType;
	
	private String color;
	
	private String sellerId;
	
	private String brand;
	
	private Integer replacementPolicy;
	
	private Double originalPrice;

	private Double discount;
	
	private Double sellingPrice;
	
	private String photoPath;

	
	
}
