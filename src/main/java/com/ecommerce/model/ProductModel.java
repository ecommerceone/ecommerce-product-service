package com.ecommerce.model;

import lombok.Data;

@Data
public class ProductModel {

	private String productName;
	
	private String productDescription;
	
	private String category;
	
	private String color;
	
	private String sellerId;
	
	private String brand;
	
	private Integer replacementPolicy;
	
	private Double originalPrice;

	private Double discount;
	
	private Double sellingPrice;
	
	private String photoPath;
	
}
