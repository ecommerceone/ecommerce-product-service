package com.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.entity.ProductInfo;

@Repository
public interface ProductRepository extends JpaRepository<ProductInfo, Long>{

}
