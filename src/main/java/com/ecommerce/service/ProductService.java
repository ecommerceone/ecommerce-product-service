package com.ecommerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.entity.ProductInfo;
import com.ecommerce.model.ProductModel;
import com.ecommerce.repository.ProductRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProductService {

	
	@Autowired
	ProductRepository productRepository;
	

	public ProductInfo addProduct(ProductModel productModel){
		try {
			log.info("Inside AddProduct Service");
			ProductInfo productInfo = transformProductRequest(productModel);
			ProductInfo savedProductInfo = productRepository.save(productInfo);
			return savedProductInfo;
		}catch(Exception e) {
			log.info(e.getMessage());
			return null;
		}
	}
	
	ProductInfo transformProductRequest(ProductModel productModel){
		ProductInfo productInfo = new ProductInfo();
		BeanUtils.copyProperties(productModel, productInfo);
		return productInfo;
	}
	
	public List<ProductInfo> getAllProducts() {
		return productRepository.findAll();
	}
	
	public ProductInfo getProductById(Long productId) {
		Optional<ProductInfo> productInfoOptional = productRepository.findById(null);
		if(productInfoOptional.isPresent()) {
			return productInfoOptional.get();
		}else {
			return null;
		}
	}
}
